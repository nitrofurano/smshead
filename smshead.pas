program SMSHead;
const
  PROG_INFO='SMSHEAD v0.2 by Haroldo O. Pinheiro';
  PROG_DESC='Pads a Sega Master System ROM to 32k and writes the required header.';
  PROG_USAGE='Usage:SMSHEAD <sms file>';

{shamefully patched by Paulo Silva ;p }

type SegaHeader=record
  Signature:array [0..7] of char;
  Unknown:Word;
  CheckSum:Word;
  PartNumber:Word;
  Version:Byte;
  CheckSumRange:Byte;
  end;

var
  F:File;
  Buf:array[0..$7FFF] of byte;
  NumRead:Word;
  fsiz:Word;
  sizkd:Word;
  ckrng:Byte;

procedure WriteHeader (qrmsiz:Word; qckrng:Byte );
  const SIG:String[8]='TMR SEGA';
  var Head:SegaHeader;i:Word;
  begin
    Move(SIG[1],Head.Signature,8);
    Head.Unknown:=$FFFF;
    Head.CheckSum:=0;
    Head.PartNumber:=$42FF;
    Head.Version:=$20;
    Head.CheckSumRange:=qckrng;
    for i:=0 to (qrmsiz-17) do
      Inc(Head.CheckSum,Buf[i]);
    Move(Head,Buf[qrmsiz-16],SizeOf(Head));
    end;

Begin
  if ParamCount<1 then
    begin
      Writeln(PROG_INFO);
      Writeln(PROG_DESC);
      Writeln(PROG_USAGE);
      Halt(1);
    end;

{$I-}
Assign(F,ParamStr(1));
Reset(F,1);
{$I+}

if IOResult <> 0 then
  begin
    Writeln(PROG_INFO);
    Writeln('File ',ParamStr(1),' not found.');
    Halt(2);
    end;

fsiz:=FileSize(F);
Writeln('file size:',fsiz);

sizkd:=32;ckrng:=$4C;
if fsiz<=16384 then
  begin
    sizkd:=16;ckrng:=$4B;
    end;
if fsiz<=8192 then
  begin
    sizkd:=8;ckrng:=$4A;
    end;

FillChar(Buf,SizeOf(Buf),0);
BlockRead(F,Buf,SizeOf(Buf),NumRead);

WriteHeader((sizkd*1024),ckrng);

Seek(F,0);
BlockWrite(F,Buf,sizkd*1024);

Close(F);
Writeln('Done.');
End.
