# smshead
fixes cartridge rom headers for Sega Master System (SMS)

usage:
smshead [file to be patched]

compile:
fpc smshead.pas

install:
sudo cp smshead /usr/bin/
